//
//  WzoryPageViewController.swift
//  MathMickv2
//
//  Created by Jimmy Phoaq on 10/10/2018.
//  Copyright © 2018 MacBook Air. All rights reserved.
//

import UIKit

class WzoryPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    lazy var subViewControlles:[UIViewController] = {
        
      return
        [
            ViewController1(),
            ViewController2(),
            ViewController3(),
            ViewController4(),
            ViewController5(),
            ViewController6(),
            ViewController7(),
            ViewController8(),
            ViewController9(),
            ViewController10(),
            ViewController11(),
            ViewController12(),
            ViewController13(),
            ViewController14(),
            ViewController15(),
            ViewController16(),
            ViewController17(),
            ViewController18(),
            ViewController19(),
            ViewController20(),
            ViewController21(),
            ViewController22(),
            ViewController23(),
            
        ]
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Theme.current.mainColor

        if revealViewController()  != nil {
            let button: UIButton = UIButton(type: .custom)
            button.setImage(UIImage(named: "menu-2"), for: UIControl.State.normal)
            button.widthAnchor.constraint(equalToConstant: 23).isActive = true
            button.heightAnchor.constraint(equalToConstant: 23).isActive = true
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self.revealViewController(), action: #selector(SWRevealViewController().revealToggle(_:)), for: .touchUpInside)
            
            let menuBtn = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = menuBtn
            
            
            //self.view.addGestureRecognizer((revealViewController()?.panGestureRecognizer())!)
        }
        
        self.delegate = self
        self.dataSource = self
        
        self.setViewControllers( [subViewControlles[0]], direction: .forward, animated: true, completion: nil)
        
      
        
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return subViewControlles.count
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subViewControlles.index(of: viewController) ?? 0
        if(currentIndex <= 0) {
            return nil
        }
        return subViewControlles[currentIndex-1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subViewControlles.index(of: viewController) ?? 0
        if (currentIndex >= subViewControlles.count - 1) {
            
            return nil
        }
        return subViewControlles[currentIndex+1]
    }
    

  

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
