//
//  ONasViewController.swift
//  MathMickV3
//
//  Created by Mati on 18.10.2018.
//  Copyright © 2018 MathMick Company. All rights reserved.
//

import UIKit

class ONasViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Theme.current.mainColor

        if revealViewController()  != nil {
            let button: UIButton = UIButton(type: .custom)
            button.setImage(UIImage(named: "menu-2"), for: UIControl.State.normal)
            button.widthAnchor.constraint(equalToConstant: 23).isActive = true
            button.heightAnchor.constraint(equalToConstant: 23).isActive = true
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self.revealViewController(), action: #selector(SWRevealViewController().revealToggle(_:)), for: .touchUpInside)
            
            let menuBtn = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = menuBtn
            
            //menuBtn.target = self.revealViewController()
            //menuBtn.action = #selector(SWRevealViewController().revealToggle(_:))
            
            self.view.addGestureRecognizer((revealViewController()?.panGestureRecognizer())!)
        }
        
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 100))
        label.numberOfLines = 5
        label.center = CGPoint(x: view.center.x, y: view.center.y + 120)
        label.textAlignment = .center
        label.text = "nnnnnnnnn \n mmmmmmm"
        self.view.addSubview(label)
        
        let imageName = "logo_black.pdf"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        
        imageView.frame = CGRect(x: view.center.x - 100, y: view.center.y - 190, width: 200, height: 200 )
        view.addSubview(imageView)

        
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
