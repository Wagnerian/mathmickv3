//
//  RankingiViewController.swift
//  MathMickV3
//
//  Created by Mati on 18.10.2018.
//  Copyright © 2018 MathMick Company. All rights reserved.
//

import UIKit



class RankingiViewController: UIViewController {
    
    var timer = Timer()
    var counts = UserDefaults.standard.integer(forKey: "Stoper")

    
    //funkcja zatrzymuje stoper kiedy VC znika z ekranu 
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
        UserDefaults.standard.set(counts, forKey: "Stoper")

    }
    
    @objc func stoppingTheTimer(notification : NSNotification) {
        print("stop method called")
        timer.invalidate()
        UserDefaults.standard.set(counts, forKey: "Stoper")

        //You may call your action method here, when the application did enter background.
        //ie., self.pauseTimer() in your case.
    }
    @objc func startingTheTimer(notification : NSNotification) {
        print("start method called")

        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            
            self.counts += 1
            
            print(self.counts)
            
            //zapisać największy runCount i dodać do danych statystyk przy konkretnej kategorii z pytania
            
            
            
        }
        //You may call your action method here, when the application did enter background.
        //ie., self.pauseTimer() in your case.
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Theme.current.mainColor

        UserDefaults.standard.set(0, forKey: "Stoper")

        NotificationCenter.default.addObserver(self, selector: #selector(stoppingTheTimer), name:UIApplication.didEnterBackgroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(startingTheTimer), name:UIApplication.didBecomeActiveNotification, object: nil)

        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            
            self.counts += 1
            
            print(self.counts)
            
            //zapisać największy runCount i dodać do danych statystyk przy konkretnej kategorii z pytania
            
            
            
        }

        if revealViewController()  != nil {
            let button: UIButton = UIButton(type: .custom)
            button.setImage(UIImage(named: "menu-2"), for: UIControl.State.normal)
            button.widthAnchor.constraint(equalToConstant: 23).isActive = true
            button.heightAnchor.constraint(equalToConstant: 23).isActive = true
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self.revealViewController(), action: #selector(SWRevealViewController().revealToggle(_:)), for: .touchUpInside)
            
            let menuBtn = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = menuBtn
            
            //menuBtn.target = self.revealViewController()
            //menuBtn.action = #selector(SWRevealViewController().revealToggle(_:))
            
            self.view.addGestureRecognizer((revealViewController()?.panGestureRecognizer())!)
        }    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
