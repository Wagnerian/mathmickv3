//
//  PakietPlusViewController.swift
//  MathMickV3
//
//  Created by Mati on 18.10.2018.
//  Copyright © 2018 MathMick Company. All rights reserved.
//

import UIKit
import CoreData

class PakietPlusViewController: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Theme.current.mainColor

        if revealViewController()  != nil {
            let button: UIButton = UIButton(type: .custom)
            button.setImage(UIImage(named: "menu-2"), for: UIControl.State.normal)
            button.widthAnchor.constraint(equalToConstant: 23).isActive = true
            button.heightAnchor.constraint(equalToConstant: 23).isActive = true
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self.revealViewController(), action: #selector(SWRevealViewController().revealToggle(_:)), for: .touchUpInside)
            
            let menuBtn = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = menuBtn
            
            //menuBtn.target = self.revealViewController()
            //menuBtn.action = #selector(SWRevealViewController().revealToggle(_:))
            
            self.view.addGestureRecognizer((revealViewController()?.panGestureRecognizer())!)
        }
        
        /*
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Algebra")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context!.fetch(request)
            for data in result as! [NSManagedObject] {
                print(data.value(forKey: "goodAnswerCounter") as! Int64)
                print(data.value(forKey: "badAnswerCounter") as! Int64)

            }
            
        } catch {
            
            print("Failed")
        }*/
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}
