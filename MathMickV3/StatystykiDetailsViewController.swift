//
//  StatystykiDetailsViewController.swift
//  MathMickv2
//
//  Created by Admin on 07.10.2018.
//  Copyright © 2018 MacBook Air. All rights reserved.
//

import UIKit

class StatystykiDetailsViewController: UIViewController {
    
    
    @IBOutlet weak var viewToScroll: UIView!

    var percent:[UILabel] = [UILabel(),UILabel(),UILabel(),UILabel(),UILabel()]
    var cat:[UILabel] = [UILabel(),UILabel(),UILabel(),UILabel(),UILabel()]
    var counterLabel:[UILabel] = [UILabel(),UILabel(),UILabel(),UILabel(),UILabel()]
    var timeLabel:[UILabel] = [UILabel(),UILabel(),UILabel(),UILabel(),UILabel()]
    var circle:[CAShapeLayer] = [CAShapeLayer(),CAShapeLayer(),CAShapeLayer(),CAShapeLayer(),CAShapeLayer()]
    var smallcircle:[CAShapeLayer] = [CAShapeLayer(),CAShapeLayer(),CAShapeLayer(),CAShapeLayer(),CAShapeLayer()]
    var T = Double()
    var A = Double()
    var P = Double()
    var F = Double()
    var G = Double()
    let AbadAnswerCounter = UserDefaults.standard.integer(forKey: "AbadAnswerCounter")
    let AgoodAnswerCounter = UserDefaults.standard.integer(forKey: "AgoodAnswerCounter")
    let FgoodAnswerCounter = UserDefaults.standard.integer(forKey: "FgoodAnswerCounter")
    let FbadAnswerCounter = UserDefaults.standard.integer(forKey: "FbadAnswerCounter")
    let TgoodAnswerCounter = UserDefaults.standard.integer(forKey: "TgoodAnswerCounter")
    let TbadAnswerCounter = UserDefaults.standard.integer(forKey: "TbadAnswerCounter")
    let GgoodAnswerCounter = UserDefaults.standard.integer(forKey: "GgoodAnswerCounter")
    let GbadAnswerCounter = UserDefaults.standard.integer(forKey: "GbadAnswerCounter")
    let PgoodAnswerCounter = UserDefaults.standard.integer(forKey: "PgoodAnswerCounter")
    let PbadAnswerCounter = UserDefaults.standard.integer(forKey: "PbadAnswerCounter")
    var goodanswers: [Double] = []    
    let categ : [String] = ["Trygonomteria","Algebra","Prawdopodobieństwo","Funkcje","Geometria"]

    let cells: [UIView] = [UIView(),UIView(),UIView(),UIView(),UIView()]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        
        if revealViewController()  != nil {
            let button: UIButton = UIButton(type: .custom)
            button.setImage(UIImage(named: "menu-2"), for: UIControl.State.normal)
            button.widthAnchor.constraint(equalToConstant: 23).isActive = true
            button.heightAnchor.constraint(equalToConstant: 23).isActive = true
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self.revealViewController(), action: #selector(SWRevealViewController().revealToggle(_:)), for: .touchUpInside)
            
            let menuBtn = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = menuBtn
            
            //menuBtn.target = self.revealViewController()
            //menuBtn.action = #selector(SWRevealViewController().revealToggle(_:))
            
            self.view.addGestureRecognizer((revealViewController()?.panGestureRecognizer())!)
        }
        T = Double(TgoodAnswerCounter)/(Double(TgoodAnswerCounter + TbadAnswerCounter))
        A = Double(AgoodAnswerCounter)/(Double(AgoodAnswerCounter+AbadAnswerCounter))
        P = Double(PgoodAnswerCounter)/(Double(PgoodAnswerCounter+PbadAnswerCounter))
        F = Double(FgoodAnswerCounter)/(Double(FgoodAnswerCounter+FbadAnswerCounter))
        G = Double(GgoodAnswerCounter)/(Double(GgoodAnswerCounter+GbadAnswerCounter))
        
       /* if T.isNaN == true {
            T = 0
        }
        if A.isNaN == true{
            A = 0
        }
        if P.isNaN == true {
            P = 0
        }
        if F.isNaN == true{
            F = 0
        }
        if G.isNaN == true {
            G = 0
        }
      */  goodanswers = [ T, A , P , F , G ]
        let answers = [TgoodAnswerCounter + TbadAnswerCounter,AgoodAnswerCounter + AbadAnswerCounter,PgoodAnswerCounter + PbadAnswerCounter,FgoodAnswerCounter + FbadAnswerCounter,GgoodAnswerCounter + GbadAnswerCounter]
        
        let dispx = view.center.x * 2
        
        let scy = viewToScroll.center.y * 2

        
    
        let place3 : [[CGFloat]] = [[dispx / 4 , scy * 1.5/10.5],[dispx / 4 , scy * 3.5/10.5],[dispx / 4, scy * 5.5 / 10.5], [dispx / 4 , scy * 7.5/10.5],[dispx / 4 , scy * 9.5/10.5], ]

        
        
        
        let rad1 = dispx * 3 * 0.9 / 32   // 3/32         // 2/32             // 10/128
        let rad2 = dispx * 33 * 0.9 / 192  // 33/192       // 22/192           // 55/384
    

        
        view.backgroundColor = Theme.current.navBarColor
        viewToScroll.backgroundColor = Theme.current.navBarColor
        
        
        Swipe()
    
      
            
            for index in 0...4{
                createCircle(category: categ[index], circlecenterx: place3[index][0], circlecentery: place3[index][1], radius1: rad1, radius2: rad2, percentrate: goodanswers[index], allanswers: answers[index],i: index)
            }
        
 
    
    }
    
    
    func Swipe(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action:  #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    func createCircle(category : String, circlecenterx: CGFloat, circlecentery: CGFloat, radius1 : CGFloat, radius2 : CGFloat, percentrate: Double, allanswers: Int, i: Int ) {
        
        
        cat[i] = {
            let lbl = UILabel()
            lbl.textAlignment = .center
            lbl.font = UIFont.boldSystemFont(ofSize: 16)
            lbl.text = category
            lbl.textColor = UIColor(white: 0, alpha: 1)
            lbl.alpha = 0
            lbl.layer.borderColor = #colorLiteral(red: 0.04100571066, green: 0.04100571066, blue: 0.04100571066, alpha: 1)
            lbl.layer.borderWidth = 0.5
            lbl.layer.cornerRadius = 11
            return lbl
        }()
        
        viewToScroll.addSubview(cells[i])
        viewToScroll.addSubview(cat[i])
        cells[i].backgroundColor = Theme.current.mainColor
        cells[i].layer.cornerRadius = 10.0
        cells[i].frame = CGRect(x: 0, y: circlecentery , width: view.center.x * 2 - 20, height: viewToScroll.center.y * 2 / 10.5 * 2 - 10)
        cells[i].center.x = view.center.x
        cells[i].center.y = circlecentery - 20
        
        cat[i].frame = CGRect(x: 0, y: circlecentery - radius1 * 2 - 40 , width: CGFloat(category.count * 12), height: 26)
        cat[i].center.x = view.center.x
        
        let center = CGPoint(x: circlecenterx, y: circlecentery)

        
        if !percentrate.isNaN {
            
        percent[i] = {
            let lbl = UILabel()
            lbl.textAlignment = .left
            lbl.font = UIFont.boldSystemFont(ofSize: 10)
            lbl.text = "Poprawnie: \(round(percentrate * 1000)/10)%"
            lbl.textColor = UIColor(white: 0, alpha: 1)
            lbl.alpha = 0
            return lbl
        }()
        
        timeLabel[i] = {
            let lbl = UILabel()
            lbl.textAlignment = .left
            lbl.font = UIFont.boldSystemFont(ofSize: 10)
            lbl.text = "Średni czas odpowiedzi - ???"
            lbl.textColor = UIColor(white: 0, alpha: 1)
            lbl.alpha = 0
            return lbl
        }()
        
        counterLabel[i] = {
            let lbl = UILabel()
            lbl.textAlignment = .left
            lbl.font = UIFont.boldSystemFont(ofSize: 10)
            lbl.text = "Ilość odpowiedzi - \(allanswers)"
            lbl.textColor = UIColor(white: 0, alpha: 1)
            lbl.alpha = 0
            return lbl
        }()
     
        
        let progress = UIBezierPath(arcCenter: .zero, radius: radius1, startAngle:  CGFloat.pi  / 2, endAngle:  ( CGFloat.pi / 2) + ( CGFloat.pi * 2), clockwise: true)
        circle[i].path = progress.cgPath
        let back = UIBezierPath(arcCenter: .zero, radius: radius2, startAngle:   CGFloat.pi  / 2, endAngle:  ( CGFloat.pi / 2) + (  CGFloat.pi * 2), clockwise: true)
        smallcircle[i].path = back.cgPath
      
      
        
        viewToScroll.layer.addSublayer(smallcircle[i])
        viewToScroll.layer.addSublayer(circle[i])
        viewToScroll.addSubview(percent[i])
        viewToScroll.addSubview(counterLabel[i])
        viewToScroll.addSubview(timeLabel[i])

        
        circle[i].fillColor = UIColor(white: 1, alpha: 0).cgColor
        smallcircle[i].fillColor = UIColor(white: 0.8, alpha: 1).cgColor
        circle[i].strokeColor = UIColor(red: 60/255, green: 179/255, blue: 113/255, alpha: 1).cgColor
        circle[i].lineWidth = radius1 * 2
        circle[i].strokeStart = 0
        circle[i].strokeEnd = CGFloat(percentrate)
        circle[i].position = center
        smallcircle[i].position = center
        percent[i].frame = CGRect(x: circlecenterx * 2, y: circlecentery - 5 , width: 200, height: 10)
        counterLabel[i].frame = CGRect(x: circlecenterx * 2, y: circlecentery - radius1 , width: 200, height: 10)
        timeLabel[i].frame = CGRect(x: circlecenterx * 2, y: circlecentery + radius1  - 10, width: 200, height: 10)
        
       
    
        
        circle[i].transform = CATransform3DMakeScale(0, 0, 1.0)
        smallcircle[i].transform = CATransform3DMakeScale(0, 0, 1.0)
        
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.fromValue = 0
        animation.toValue = 1
        animation.duration = 1
        animation.beginTime = CACurrentMediaTime() + 0.25 + Double(i)/8
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        animation.timingFunction = CAMediaTimingFunction(controlPoints: 5/6, 0.2, 2/6, 0.9)
        circle[i].add(animation, forKey: " ")
        smallcircle[i].add(animation, forKey: " ")
            
        }
        
        else {
            let warninglabel:UILabel = {
                let lbl = UILabel()
                lbl.textAlignment = .center
                lbl.font = UIFont.boldSystemFont(ofSize: 13)
                lbl.text = "Nie odpowiedziałeś jeszcze na żadne pytanie z kategorii \(category). Przejdź do quizu."
                lbl.textColor = UIColor(white: 0, alpha: 1)
                lbl.alpha = 1
                lbl.contentMode = .scaleToFill
                lbl.lineBreakMode = .byWordWrapping
                lbl.numberOfLines = 0

                return lbl
            }()
            warninglabel.frame = CGRect(x: 0, y: 0, width: 200, height: 100)
            viewToScroll.addSubview(warninglabel)
            warninglabel.center.x = view.center.x
            warninglabel.center.y = circlecentery

        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        for index in 0...4{
            
            UIView.animate(withDuration: 1, delay: 0.75 + TimeInterval(index)/8, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.percent[index].alpha = 1
                self.cat[index].alpha = 1
                self.counterLabel[index].alpha = 1
                self.timeLabel[index].alpha = 1
            })
        }
    }
    
    
    @objc func respondToSwipeGesture(_ recognizer: UISwipeGestureRecognizer) {
        //let panel: UIViewController = PanelStartowy()
        //present(panel, animated: false, completion: nil)
        presentingViewController?.dismiss(animated: false, completion: nil)
        
    }
    
}
