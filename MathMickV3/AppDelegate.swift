//
//  AppDelegate.swift
//  MathMickV3
//
//  Created by Mati on 18.10.2018.
//  Copyright © 2018 MathMick Company. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let defaults = UserDefaults.standard
    
    var restrictRotation:UIInterfaceOrientationMask = .portrait
    var result1: [Array<String>] = []
    var result2: [Array<String>] = []
    var result3: [Array<String>] = []
    var result4: [Array<String>] = []
    var result5: [Array<String>] = []
    var results: [Array<Array<String>>] = [[[]]]
    var coreDataQuestionArrays: [[Int]] = [[]]
    static var persistentContainer: NSPersistentContainer {
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer
    }
    
    static var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    
    let categories = ["Algebra" , "Funkcje" , "Trygonometria" , "Geometria" , "Prawdopodobienstwo"]

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        isAppAlreadyLaunchedOnce()
        Theme.current.apply()
       // UINavigationBar.appearance().backgroundColor = UIColor.green

        //UIApplication.shared.statusBarStyle = .default
        
        //SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask
    {
        return self.restrictRotation
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    //////////////////////////// FACEBOOK ACCESS ///////////////////////////////////////////
    
    
    /*func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
     print("URL AbsoluteString = \(url.absoluteString)")
     
     if url.absoluteString.contains("2319955524899406") {
     return SDKApplicationDelegate.shared.application(app, open: url, options: options)
     } else {
     debugPrint("Error in openURL from AppDelegate Facebook Sign in")
     }
     
     return true
     
     }*/
    
    /////////////////////////////////////////////////////////////////////////////////////////
    
    func isAppAlreadyLaunchedOnce()->Bool{
        if let _ = defaults.string(forKey: "isAppAlreadyLaunchedOnce"){
            print("App already launched")
            return true
        }else{
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
            defaults.set(0, forKey: "AgoodAnswerCounter")
            defaults.set(0, forKey: "AbadanswerCounter")
            defaults.set(0, forKey: "FgoodAnswerCounter")
            defaults.set(0, forKey: "FbadanswerCounter")
            defaults.set(0, forKey: "TgoodAnswerCounter")
            defaults.set(0, forKey: "TbadanswerCounter")
            defaults.set(0, forKey: "GgoodAnswerCounter")
            defaults.set(0, forKey: "GbadanswerCounter")
            defaults.set(0, forKey: "PgoodAnswerCounter")
            defaults.set(0, forKey: "PbadanswerCounter")
            defaults.set(false, forKey: "AlgebraCategory")
            
            print("App launched first time")
            
            
            for category in categories {
                generatorPytan(category)
            }
            
            for category in categories {
                print(category)
                fetchFromCoreData(category , "questions")
            }
            coreDataQuestionArrays.remove(at: 0)
            print(coreDataQuestionArrays)
            
            return false
        }
    }
    
    func generatorPytan(_ kategoria: String) {
        odczyt()
        
        if kategoria == categories[0] {
            let losowanie = GenerateRandomNumbers(1, results[0].count, (results[0].count - 1))
            saveInCoreData("Algebra", losowanie , "questions")
        }
        else if kategoria == categories[1] {
            let losowanie = GenerateRandomNumbers(1, results[1].count, (results[1].count - 1))
            saveInCoreData("Funkcje", losowanie , "questions")
        }
        else if kategoria == categories[2] {
            let losowanie = GenerateRandomNumbers(1, results[2].count, (results[2].count - 1))
            saveInCoreData("Trygonometria", losowanie , "questions")
        }
        else if kategoria == categories[3] {
            let losowanie = GenerateRandomNumbers(1, results[3].count, (results[3].count - 1))
            saveInCoreData("Geometria", losowanie , "questions")
        }
        else if kategoria == categories[4] {
            let losowanie = GenerateRandomNumbers(1, results[4].count, (results[4].count - 1))
            saveInCoreData("Prawdopodobienstwo", losowanie , "questions")
        }
    }
    
    
    //////////////////      Czytanie z pliku pytań i odpowiedzi  tablica[][]   ////////////////////////////////////
    
    func odczyt() {
        
        results = [result1 , result2 , result3 , result4 , result5]
        for kategoria in categories {
            
            let fileURL = Bundle.main.path(forResource: kategoria, ofType: "txt")
            var readString = ""
            do {
                readString = try String(contentsOfFile: fileURL! , encoding: String.Encoding.utf8)
                
                let myStrings = readString.components(separatedBy: "\n")
                for row in myStrings {
                    let columns = row.components(separatedBy: "#")
                    
                    if kategoria == categories[0] {
                        results[0].append(columns)
                    }
                    else if kategoria == categories[1] {
                        results[1].append(columns)
                    }
                    else if kategoria == categories[2] {
                        results[2].append(columns)
                    }
                    else if kategoria == categories[3] {
                        results[3].append(columns)
                    }
                    else if kategoria == categories[4] {
                        results[4].append(columns)
                    }
                }
            }
            catch let error as NSError {
                print("failed to read from file bazapytan")
                print(error)
            }
        }
        
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    func saveInCoreData(_ entityName: String , _ value: Any , _ attributeName: String) {
        
        let context = AppDelegate.viewContext
        
        let entity = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context)
        entity.setValue(value, forKey: attributeName)
        
        let delegate = UIApplication.shared.delegate as? AppDelegate
        
        do {
            try delegate?.saveContext()
        }
        catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
    }
    
    func fetchFromCoreData(_ entityName: String , _ attributeName: String) {
        let context = AppDelegate.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                coreDataQuestionArrays.append(data.value(forKey: attributeName) as! [Int])
                print(" dawaj info: \(data.value(forKey: attributeName) as! [Int])")
            }
        }
        catch let error as NSError {
            print("Failed to fetch due to \(error), \(error.userInfo)")
        }
    }
    
    func ClearCoreData(_ entity: String) {
        let delegate = UIApplication.shared.delegate as? AppDelegate
        
        let moc = delegate?.persistentContainer.viewContext
        
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entity)
        do {
            
            if let allObjects = try moc?.fetch(request) as? [NSManagedObject] {
                
                for object in allObjects {
                    moc?.delete(object)
                }
            }
        }
        catch let error as NSError {
            print(error)
        }
        
    }
    
    
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "MathMickV3")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func GenerateRandomNumbers(_ from: Int, _ to: Int, _ howmany: Int?) -> [Int] {
        var myRandomNumbers: [Int] = []
        var numberOfNumbers = howmany
        
        let lower = UInt32(from)
        let higher = UInt32(to)
        
        if numberOfNumbers == nil || numberOfNumbers! > (to - from) + 1 {
            numberOfNumbers = (to - from) + 1
        }
        
        while myRandomNumbers.count != numberOfNumbers {
            let myNumber = arc4random_uniform(higher - lower) + lower
            
            if !myRandomNumbers.contains(Int(myNumber)) {
                myRandomNumbers.append(Int(myNumber))
            }
        }
        
        return myRandomNumbers
        
    }
    
    
}

