//
//  PopAnimator.swift
//  MathMick
//
//  Created by MacBook Air on 01/10/2018.
//  Copyright © 2018 MacBook Air. All rights reserved.
//

import UIKit


class PopAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration = 1.0
    var presenting = true
    var originFrame = CGRect.zero
    //var dismissCompletion: (()->Void)?
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView
        let toView = transitionContext.view(forKey: .to)!
        
        //let herbView = presenting ? toView :
        //  transitionContext.view(forKey: .from)!
        
        containerView.addSubview(toView)
        toView.alpha = 0.0
        UIView.animate(withDuration: duration,
                       animations: {
                        toView.alpha = 1.0
        },
                       completion: { _ in
                        
                        transitionContext.completeTransition(true)
        }
        )
    }
    
    
}

