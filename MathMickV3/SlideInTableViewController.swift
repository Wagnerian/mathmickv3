//
//  SlideInTableViewController.swift
//  MathMickV3
//
//  Created by Mati on 18.10.2018.
//  Copyright © 2018 MathMick Company. All rights reserved.
//

import UIKit
import CoreData
import MessageUI

class SlideInTableViewController: UIViewController, UITableViewDataSource , UITableViewDelegate , MFMailComposeViewControllerDelegate {
    
    struct cellData {
        var opened = Bool()
        var title = String()
        var sectionData = [String]()
    }
    var opcje = [cellData]()
    var observation : NSKeyValueObservation?

    let defaults = UserDefaults.standard
    let categories = ["Algebra" , "Funkcje" , "Trygonometria" , "Geometria" , "Prawdopodobienstwo"]
    let mainCategories = ["Menu Główne" , "Ucz się!" , "Wzory Maturalne" , "Motywy" , "Statystyki" , "Kategoria" , "Feedback" , "O Nas" , "Wyzeruj"]
    let images = ["menu-1" , "books-stack-of-three-4" , "blackboard" , "artistic-brush" , "pie-chart" , "list" , "feedback-1" , "multiple-users-silhouette" , "delete-button"]
    let goodCounterStrings = ["AgoodAnswerCounter" , "FgoodAnswerCounter" , "TgoodAnswerCounter" , "GgoodAnswerCounter" , "PgoodAnswerCounter"]
    let badCounterStrings = ["AbadAnswerCounter" , "FbadAnswerCounter" , "TbadAnswerCounter" , "GbadAnswerCounter" , "PbadAnswerCounter"]
    let CoreDataContainer = AppDelegate.persistentContainer
    let context = AppDelegate.viewContext
    let delegate = UIApplication.shared.delegate as? AppDelegate
    
    
    @IBOutlet weak var tableView: UITableView! { didSet
    { tableView.delegate = self
      tableView.dataSource = self } }
    var size =  CGFloat()
    var coreDataQuestionArrays: [Array<Int>] = []
    var result1: [Array<String>] = []
    var result2: [Array<String>] = []
    var result3: [Array<String>] = []
    var result4: [Array<String>] = []
    var result5: [Array<String>] = []
    var results: [Array<Array<String>>] = [[[]]]
    var imageViewAlgebra = UIImageView()
    var imageViewFunkcje = UIImageView()
    var imageViewTrygonometria = UIImageView()
    var imageViewGeometria = UIImageView()
    var imageViewPrawdopodobienstwo = UIImageView()
    
    @IBOutlet weak var ZerowanieView: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.revealViewController().frontViewController.view.isUserInteractionEnabled = false
    self.revealViewController().view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        setSliderWidth()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.revealViewController().frontViewController.view.isUserInteractionEnabled = true
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = Theme.current.mainColor
        tableView.backgroundColor = Theme.current.mainColor
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        opcje = [cellData(opened: false, title: "Menu Główne", sectionData: []) ,
                 cellData(opened: false, title: "Ucz się!", sectionData: []) ,
                 cellData(opened: false, title: "Wzory Maturalne", sectionData: []) ,
                 //cellData(opened: false, title: "Pakiet +", sectionData: []) ,
                 cellData(opened: false, title: "Motywy", sectionData: []) ,
                 //cellData(opened: false, title: "Rankingi", sectionData: []) ,
                 cellData(opened: false, title: "Progres", sectionData: []) ,
                 cellData(opened: false, title: "Kategorie", sectionData: ["Algebra" , "Funkcje" , "Trygonometria" , "Geometria" , "Prawdopodobienstwo"]) ,
                 //cellData(opened: false, title: "Facebook", sectionData: []) ,
                 cellData(opened: false, title: "Feedback", sectionData: []) ,
                 cellData(opened: false, title: "O Nas", sectionData: []) ,
                 cellData(opened: false, title: "Wyzeruj", sectionData: [])]
        
    }
    
    // MARK: SETTING UP WIDTH OF REVEALING MENU
    
    func setSliderWidth() {

            if UIDevice.current.userInterfaceIdiom == .phone {
                self.revealViewController()?.rearViewRevealWidth = self.view.center.x*2*8/10
            }
            else if UIDevice.current.userInterfaceIdiom == .pad{
                self.revealViewController()?.rearViewRevealWidth = self.view.center.x*2*5/10
            }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return opcje.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if opcje[section].opened == true {
            return opcje[section].sectionData.count + 1
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (UIDevice.current.orientation.isPortrait == true) {
            size = self.view.center.y * 2 * 11 / 100
        }
        else if(UIDevice.current.orientation.isLandscape == true) {
            size = self.view.center.y * 2 * 15 / 100
            
        }
        return size
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: opcje[indexPath.section].title) else {return UITableViewCell()}
            cell.textLabel?.text = "\t\(mainCategories[indexPath.section])"
            cell.imageView?.image = UIImage(named: images[indexPath.section])
            return cell
        }
        else if indexPath.row == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Algebra") else {return UITableViewCell()}
            cell.backgroundColor = Theme.current.categoryBackColor
            imageViewAlgebra = cell.imageView!
            imageViewAlgebra.image = UIImage(named: "tick")
            cell.addSubview(imageViewAlgebra)
            imageViewAlgebra.translatesAutoresizingMaskIntoConstraints = false
            imageViewAlgebra.widthAnchor.constraint(equalToConstant: 24).isActive = true
            imageViewAlgebra.heightAnchor.constraint(equalToConstant: 24).isActive = true
            imageViewAlgebra.leftAnchor.constraint(equalTo: cell.leftAnchor , constant: 215).isActive = true
            imageViewAlgebra.topAnchor.constraint(equalTo: cell.topAnchor , constant: 20).isActive = true
            imageViewAlgebra.isHidden = defaults.bool(forKey: "AlgebraCategory")
            
            return cell
        }
        else if indexPath.row == 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Funkcje") else {return UITableViewCell()}
            cell.backgroundColor = Theme.current.categoryBackColor
            imageViewFunkcje = cell.imageView!
            imageViewFunkcje.image = UIImage(named: "tick")
            cell.addSubview(imageViewFunkcje)
            imageViewFunkcje.translatesAutoresizingMaskIntoConstraints = false
            imageViewFunkcje.widthAnchor.constraint(equalToConstant: 24).isActive = true
            imageViewFunkcje.heightAnchor.constraint(equalToConstant: 24).isActive = true
            imageViewFunkcje.leftAnchor.constraint(equalTo: cell.leftAnchor , constant: 215).isActive = true
            imageViewFunkcje.topAnchor.constraint(equalTo: cell.topAnchor , constant: 20).isActive = true
            imageViewFunkcje.isHidden = defaults.bool(forKey: "FunkcjeCategory")
            
            return cell
        }
        else if indexPath.row == 3 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Trygonometria") else {return UITableViewCell()}
            cell.backgroundColor = Theme.current.categoryBackColor
            imageViewTrygonometria = cell.imageView!
            imageViewTrygonometria.image = UIImage(named: "tick")
            cell.addSubview(imageViewTrygonometria)
            imageViewTrygonometria.translatesAutoresizingMaskIntoConstraints = false
            imageViewTrygonometria.widthAnchor.constraint(equalToConstant: 24).isActive = true
            imageViewTrygonometria.heightAnchor.constraint(equalToConstant: 24).isActive = true
            imageViewTrygonometria.leftAnchor.constraint(equalTo: cell.leftAnchor , constant: 215).isActive = true
            imageViewTrygonometria.topAnchor.constraint(equalTo: cell.topAnchor , constant: 20).isActive = true
            imageViewTrygonometria.isHidden = defaults.bool(forKey: "TrygonometriaCategory")
            
            return cell
        }
        else if indexPath.row == 4 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Geometria") else {return UITableViewCell()}
            cell.backgroundColor = Theme.current.categoryBackColor
            imageViewGeometria = cell.imageView!
            imageViewGeometria.image = UIImage(named: "tick")
            cell.addSubview(imageViewGeometria)
            imageViewGeometria.translatesAutoresizingMaskIntoConstraints = false
            imageViewGeometria.widthAnchor.constraint(equalToConstant: 24).isActive = true
            imageViewGeometria.heightAnchor.constraint(equalToConstant: 24).isActive = true
            imageViewGeometria.leftAnchor.constraint(equalTo: cell.leftAnchor , constant: 215).isActive = true
            imageViewGeometria.topAnchor.constraint(equalTo: cell.topAnchor , constant: 20).isActive = true
            imageViewGeometria.isHidden = defaults.bool(forKey: "GeometriaCategory")
            
            return cell
        }
        else if indexPath.row == 5 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Prawdopodobienstwo") else {return UITableViewCell()}
            cell.backgroundColor = Theme.current.categoryBackColor
            imageViewPrawdopodobienstwo = cell.imageView!
            imageViewPrawdopodobienstwo.image = UIImage(named: "tick")
            cell.addSubview(imageViewPrawdopodobienstwo)
            imageViewPrawdopodobienstwo.translatesAutoresizingMaskIntoConstraints = false
            imageViewPrawdopodobienstwo.widthAnchor.constraint(equalToConstant: 24).isActive = true
            imageViewPrawdopodobienstwo.heightAnchor.constraint(equalToConstant: 24).isActive = true
            imageViewPrawdopodobienstwo.leftAnchor.constraint(equalTo: cell.leftAnchor , constant: 215).isActive = true
            imageViewPrawdopodobienstwo.topAnchor.constraint(equalTo: cell.topAnchor , constant: 20).isActive = true
            imageViewPrawdopodobienstwo.isHidden = defaults.bool(forKey: "PrawdopodobienstwoCategory")
            
            
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if opcje[indexPath.section].opened == true {
                opcje[indexPath.section].opened = false
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            }
            else {
                opcje[indexPath.section].opened = true
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            }
            if indexPath.section == 8 {
                deleteDataAction()
            }
            else if indexPath.section == 3 {

                if Theme.current == .default {
                if let selectedTheme = Theme(rawValue: Theme.dark.rawValue) {
                    selectedTheme.apply()
                }
                    

                   let windows = UIApplication.shared.windows
                    for window in windows {
                        for view in window.subviews {
                            view.removeFromSuperview()
                            window.addSubview(view)
                        }
                    }
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let frontReload = storyboard.instantiateViewController(withIdentifier: "menuNavigation")
                    revealViewController()?.setFront(frontReload, animated: false)
                    
                    let rearReload = storyboard.instantiateViewController(withIdentifier: "navslideIn")
                    revealViewController()?.setRear(rearReload, animated: false)
                }
                else if Theme.current == .dark {
                    if let selectedTheme = Theme(rawValue: Theme.default.rawValue) {
                        selectedTheme.apply()
                    }
                    let windows = UIApplication.shared.windows
                    for window in windows {
                        for view in window.subviews {
                            view.removeFromSuperview()
                            window.addSubview(view)
                        }
                    }
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let frontReload = storyboard.instantiateViewController(withIdentifier: "menuNavigation")
                    revealViewController()?.setFront(frontReload, animated: false)
                    
                    let rearReload = storyboard.instantiateViewController(withIdentifier: "navslideIn")
                    revealViewController()?.setRear(rearReload, animated: false)
                }
            
            }
            else if indexPath.section == 6 {
            
                let alert = UIAlertController(title: "Napisz do nas lub oceń aplikację w App Store", message: "W razie jakichkolwiek błędów powiadom Nas, żeby Twoja aplikacja działała lepiej", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Napisz Maila", style: .default) { (action:UIAlertAction!) in
                    //pisanie maila
                    self.sendEmail()
                })
                alert.addAction(UIAlertAction(title: "Oceń Nas", style: .default) { (action:UIAlertAction!) in
                    //ocena w appstore
                    self.rateApp(appId: "MathMickV3") { success in
                        print("RateApp \(success)")
                    }
                })
                alert.addAction(UIAlertAction(title: "Anuluj", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }
        }
            
            
            
        else if indexPath.row == 1 {
            
            if defaults.bool(forKey: "AlgebraCategory") == false {
                defaults.set(true, forKey: "AlgebraCategory")
                imageViewAlgebra.isHidden = true
                defaults.synchronize()
            }
            else if defaults.bool(forKey: "AlgebraCategory") == true {
                defaults.set(false, forKey: "AlgebraCategory")
                imageViewAlgebra.isHidden = false
                defaults.synchronize()
            }
        }
        else if indexPath.row == 2 {
            if defaults.bool(forKey: "FunkcjeCategory") == false {
                defaults.set(true, forKey: "FunkcjeCategory")
                imageViewFunkcje.isHidden = true
                defaults.synchronize()
            }
            else if defaults.bool(forKey: "FunkcjeCategory") == true {
                defaults.set(false, forKey: "FunkcjeCategory")
                imageViewFunkcje.isHidden = false

                defaults.synchronize()
            }
        }
        else if indexPath.row == 3 {
            if defaults.bool(forKey: "TrygonometriaCategory") == false {
                defaults.set(true, forKey: "TrygonometriaCategory")
                imageViewTrygonometria.isHidden = true
                defaults.synchronize()
            }
            else if defaults.bool(forKey: "TrygonometriaCategory") == true {
                defaults.set(false, forKey: "TrygonometriaCategory")
                imageViewTrygonometria.isHidden = false
                defaults.synchronize()
            }
        }
        else if indexPath.row == 4 {
            if defaults.bool(forKey: "GeometriaCategory") == false {
                defaults.set(true, forKey: "GeometriaCategory")
                imageViewGeometria.isHidden = true
                defaults.synchronize()
            }
            else if defaults.bool(forKey: "GeometriaCategory") == true {
                defaults.set(false, forKey: "GeometriaCategory")
                imageViewGeometria.isHidden = false
                defaults.synchronize()
            }
        }
        else if indexPath.row == 5 {
            if defaults.bool(forKey: "PrawdopodobienstwoCategory") == false {
                defaults.set(true, forKey: "PrawdopodobienstwoCategory")
                imageViewPrawdopodobienstwo.isHidden = true
                defaults.synchronize()
            }
            else if defaults.bool(forKey: "PrawdopodobienstwoCategory") == true {
                defaults.set(false, forKey: "PrawdopodobienstwoCategory")
                imageViewPrawdopodobienstwo.isHidden = false
                defaults.synchronize()
            }
        }
        
        
        
        
    }
    
    func deleteDataAction() {
        let alertController = UIAlertController(title: "Czy na pewno chcesz usunąć wszystkie zapisane dane oraz postępy ?", message: "Uwaga! Cały twój dotychczasowy postęp zostanie usunięty bez mozliwości odzyskania!", preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "Wyzeruj", style: UIAlertAction.Style.destructive, handler: { action in
            for category in self.categories {
                self.ClearCoreData(category) }
            for index in 0...4 {
                self.defaults.removeObject(forKey: self.goodCounterStrings[index])
                self.defaults.removeObject(forKey: self.badCounterStrings[index])

            }
            
            self.defaults.synchronize()
            self.result1.removeAll()
            self.result2.removeAll()
            self.result3.removeAll()
            self.result4.removeAll()
            self.result5.removeAll()
            
            for kategoria in self.categories {
                self.generatorPytan(kategoria)
            }
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "menuNavigation")
            self.navigationController?.revealViewController()?.pushFrontViewController(vc, animated: true)
        }))
        alertController.addAction(UIAlertAction(title: "Anuluj", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func generatorPytan(_ kategoria: String) {
        odczyt()
        
        if kategoria == categories[0] {
            let losowanie = GenerateRandomNumbers(1, results[0].count, (results[0].count - 1))
            saveInCoreData("Algebra", losowanie , "questions")
        }
        else if kategoria == categories[1] {
            let losowanie = GenerateRandomNumbers(1, results[1].count, (results[1].count - 1))
            saveInCoreData("Funkcje", losowanie , "questions")
        }
        else if kategoria == categories[2] {
            let losowanie = GenerateRandomNumbers(1, results[2].count, (results[2].count - 1))
            saveInCoreData("Trygonometria", losowanie , "questions")
        }
        else if kategoria == categories[3] {
            let losowanie = GenerateRandomNumbers(1, results[3].count, (results[3].count - 1))
            saveInCoreData("Geometria", losowanie , "questions")
        }
        else if kategoria == categories[4] {
            let losowanie = GenerateRandomNumbers(1, results[4].count, (results[4].count - 1))
            saveInCoreData("Prawdopodobienstwo", losowanie , "questions")
        }
    }
    
    
    //////////////////      FEEDBACK ALERT CONTROLLER   ////////////////////////////////////

    
    
    func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }
    
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["solved@mail.com"])
            
            
            
            // Configure the fields of the interface.
            
            mail.setSubject("MathMick - Feedback")
            
            
            
            mail.setMessageBody("<p>W MathMicku najbardziej podoba mi się...</p>", isHTML: true)
            
            present(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    
    
    //////////////////      Czytanie z pliku pytań i odpowiedzi  tablica[][]   ////////////////////////////////////
    
    
    func odczyt() {
        
        results = [result1 , result2 , result3 , result4 , result5]
        for kategoria in categories {
            
            let fileURL = Bundle.main.path(forResource: kategoria, ofType: "txt")
            var readString = ""
            do {
                readString = try String(contentsOfFile: fileURL! , encoding: String.Encoding.utf8)
                
                let myStrings = readString.components(separatedBy: "\n")
                for row in myStrings {
                    let columns = row.components(separatedBy: "#")
                    
                    if kategoria == categories[0] {
                        results[0].append(columns)
                    }
                    else if kategoria == categories[1] {
                        results[1].append(columns)
                    }
                    else if kategoria == categories[2] {
                        results[2].append(columns)
                    }
                    else if kategoria == categories[3] {
                        results[3].append(columns)
                    }
                    else if kategoria == categories[4] {
                        results[4].append(columns)
                    }
                }
            }
            catch let error as NSError {
                print("failed to read from file bazapytan")
                print(error)
            }
        }
        
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func GenerateRandomNumbers(_ from: Int, _ to: Int, _ howmany: Int?) -> [Int] {
        var myRandomNumbers: [Int] = []
        var numberOfNumbers = howmany
        
        let lower = UInt32(from)
        let higher = UInt32(to)
        
        if numberOfNumbers == nil || numberOfNumbers! > (to - from) + 1 {
            numberOfNumbers = (to - from) + 1
        }
        
        while myRandomNumbers.count != numberOfNumbers {
            let myNumber = arc4random_uniform(higher - lower) + lower
            
            if !myRandomNumbers.contains(Int(myNumber)) {
                myRandomNumbers.append(Int(myNumber))
            }
        }
        
        return myRandomNumbers
        
    }
    
    func saveInCoreData(_ entityName: String , _ value: Any , _ attributeName: String) {
        
        let context = AppDelegate.viewContext
        
        let entity = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context)
        entity.setValue(value, forKey: attributeName)
        
        let delegate = UIApplication.shared.delegate as? AppDelegate
        
        do {
            try delegate?.saveContext()
        }
        catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
    }
    
    func fetchFromCoreData(_ entityName: String , _ attributeName: String) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                if let text = data.value(forKey: attributeName) {
                    print(text)
                    coreDataQuestionArrays.append(text as! [Int])
                    print(coreDataQuestionArrays)
                }
                //print(data.value(forKey: attributeName))
                //coreDataQuestionArrays.append(data.value(forKey: attributeName) as! [Int])
                //print(" dawaj info: \(data.value(forKey: attributeName) as! [Int])")
            }
        }
        catch let error as NSError {
            print("Failed to fetch due to \(error), \(error.userInfo)")
        }
    }
    
    func ClearCoreData(_ entity: String) {
        
        let moc = delegate?.persistentContainer.viewContext
        
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entity)
        do {
            
            if let allObjects = try moc?.fetch(request) as? [NSManagedObject] {
                
                for object in allObjects {
                    moc?.delete(object)
                }
            }
        }
        catch let error as NSError {
            print(error)
        }
        
    }
    
    func imageWithImage(image:UIImage,scaledToSize newSize:CGSize)->UIImage{
        
        UIGraphicsBeginImageContext( newSize )
        image.draw(in: CGRect(x: 0,y: 0,width: newSize.width,height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!.withRenderingMode(.alwaysOriginal)
    }
    
    func resizeImageWithAspect(image: UIImage,scaledToMaxWidth width:CGFloat,maxHeight height :CGFloat)->UIImage? {
        let oldWidth = image.size.width;
        let oldHeight = image.size.height;
        
        let scaleFactor = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
        
        let newHeight = oldHeight * scaleFactor;
        let newWidth = oldWidth * scaleFactor;
        let newSize = CGSize(width: newWidth, height: newHeight)
        
        UIGraphicsBeginImageContextWithOptions(newSize,false,UIScreen.main.scale);
        
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height));
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


