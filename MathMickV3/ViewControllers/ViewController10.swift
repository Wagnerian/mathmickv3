//
//  ViewController10.swift
//  MathMickv2
//
//  Created by Jimmy Phoaq on 11/10/2018.
//  Copyright © 2018 MacBook Air. All rights reserved.
//

import UIKit

class ViewController10: UIViewController, UIScrollViewDelegate {
    
    
    var scrollView: UIScrollView!
    var imageView: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        let navigationBarHeight =  UIApplication.shared.statusBarFrame.size.height +
        44
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 2
        view.addGestureRecognizer(tap)
        
        
        imageView = UIImageView(image: UIImage(named: "mat10.png"))
        imageView.frame = CGRect(x: 0, y: 0, width: view.center.x * 2, height: view.center.y * 2 - navigationBarHeight)
        
        
        
        scrollView = UIScrollView(frame: view.bounds)
        scrollView.contentSize = imageView.bounds.size
        scrollView.frame = CGRect(x: 0, y: 0, width: view.center.x * 2, height: view.center.y * 2 - navigationBarHeight)
        
        scrollView.delegate = self
        scrollView.addSubview(imageView)
        view.addSubview(scrollView)
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 5.0
        
        
        
        
    }
    
    @objc func doubleTapped() {
        
        if (scrollView.zoomScale > scrollView.minimumZoomScale) {
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        } else {
            scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
        }
        
    }
    
    
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    
}
