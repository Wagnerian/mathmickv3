//
//  ViewController.swift
//  MathMickV3
//
//  Created by Mati on 18.10.2018.
//  Copyright © 2018 MathMick Company. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    
    let imidz = UIImageView()
    let transition = PopAnimator()
    let categories = ["Algebra" , "Funkcje" , "Trygonometria" , "Geometria" , "Prawdopodobienstwo"]

    let beginText: UILabel = {
        let lbl = UILabel()
        lbl.text = "Jeszcze nie rozwiązałeś żadnego zadania. Przejdź do Quizu"
        lbl.textColor = UIColor.black
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        lbl.contentMode = .scaleToFill
        lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 0
        return lbl
    }()
    
    
    let Startbutton: UIButton = {
        let button = UIButton()
        button.setTitle("Ucz się! ", for: .normal)
        button.addTarget(self, action: #selector(buttonStart), for: .touchUpInside)
        button.backgroundColor = Theme.current.ButtonsBackColor
        button.setTitleColor(#colorLiteral(red: 0.8210221529, green: 0.8669400215, blue: 0.8650611639, alpha: 1), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let image1  = UIImageView()
    
    
    let label1 = UILabel()
    let label2 = UILabel()
    let label3 = UILabel()
    let label4 = UILabel()
    var line1 = CAShapeLayer()
    var line2 = CAShapeLayer()
    var line3 = CAShapeLayer()
    var circle = CAShapeLayer()
    var smallcircle = CAShapeLayer()

    let circlecolor = UIColor(red: 60/255, green: 179/255, blue: 113/255, alpha: 1).cgColor
    let black = UIColor(white: 0, alpha: 1).cgColor
    let gray = UIColor(white: 0.8, alpha: 1).cgColor
    
    let AbadAnswerCounter = UserDefaults.standard.integer(forKey: "AbadAnswerCounter")
    let AgoodAnswerCounter = UserDefaults.standard.integer(forKey: "AgoodAnswerCounter")
    let FgoodAnswerCounter = UserDefaults.standard.integer(forKey: "FgoodAnswerCounter")
    let FbadAnswerCounter = UserDefaults.standard.integer(forKey: "FbadAnswerCounter")
    let TgoodAnswerCounter = UserDefaults.standard.integer(forKey: "TgoodAnswerCounter")
    let TbadAnswerCounter = UserDefaults.standard.integer(forKey: "TbadAnswerCounter")
    let GgoodAnswerCounter = UserDefaults.standard.integer(forKey: "GgoodAnswerCounter")
    let GbadAnswerCounter = UserDefaults.standard.integer(forKey: "GbadAnswerCounter")
    let PgoodAnswerCounter = UserDefaults.standard.integer(forKey: "PgoodAnswerCounter")
    let PbadAnswerCounter = UserDefaults.standard.integer(forKey: "PbadAnswerCounter")
    var gooodAnswersNanChecker = Double()
    
    var goodanswers = Double()     //  Tej zmiennej trzeba nadac wartosc dobrych odp z coredata
    

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Theme.current.mainColor

        /*
        let request: NSFetchRequest<Algebra> = Algebra.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "czasOdp", ascending: true)
        let context = AppDelegate.viewContext
        let fetch = try? context.fetch(request)
         
*/
        /*for category in categories {
            fetchFromCoreData(category, "czasOdp")
        }*/
        
        /*let request: NSFetchRequest<Algebra> = Algebra.fetchRequest()
        let notSearch = 0
        request.predicate = NSPredicate(format: "czasOdp = %@", notSearch)
        let context = AppDelegate.viewContext
        let result = try? context.fetch(request)
        
        for czas in result! {
            print("Odp \(czas.czasOdp)")
        }*/
        
        gooodAnswersNanChecker = Double(AgoodAnswerCounter + FgoodAnswerCounter + TgoodAnswerCounter + GgoodAnswerCounter + PgoodAnswerCounter)/Double(AgoodAnswerCounter + FgoodAnswerCounter + TgoodAnswerCounter + GgoodAnswerCounter + PgoodAnswerCounter + AbadAnswerCounter + FbadAnswerCounter + TbadAnswerCounter + GbadAnswerCounter + PbadAnswerCounter)
        
        if gooodAnswersNanChecker.isNaN == true {
            gooodAnswersNanChecker = 0
            
            beginText.frame = CGRect(x: view.center.x - 100, y: view.center.y - 100, width: 200, height: 200)
            view.addSubview(beginText)
        }
        else {
            view.sendSubviewToBack(beginText)
            goodanswers = Double(gooodAnswersNanChecker)
            
                setupView()
            
        }

        view.addSubview(Startbutton)
        
        Startbutton.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        Startbutton.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        Startbutton.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        Startbutton.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1).isActive = true
        

        
        
        
        if revealViewController()  != nil {
            let button: UIButton = UIButton(type: .custom)
            button.setImage(UIImage(named: "menu-2"), for: UIControl.State.normal)
            button.widthAnchor.constraint(equalToConstant: 23).isActive = true
            button.heightAnchor.constraint(equalToConstant: 23).isActive = true
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self.revealViewController(), action: #selector(SWRevealViewController().revealToggle(_:)), for: .touchUpInside)
            
            let menuBtn = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = menuBtn
            
            
            self.view.addGestureRecognizer((revealViewController()?.panGestureRecognizer())!)
            self.revealViewController().tapGestureRecognizer()

        }
    }
    /*
    func fetchFromCoreData(_ entityName: String , _ attributeName: String) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let context = AppDelegate.viewContext
            
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                if let text = data.value(forKey: attributeName) {
                    print(text)
                    //coreDataQuestionArrays.append(text as! [Int])
                    //print(coreDataQuestionArrays)
                }
                //print(data.value(forKey: attributeName))
                //coreDataQuestionArrays.append(data.value(forKey: attributeName) as! [Int])
                //print(" dawaj info: \(data.value(forKey: attributeName) as! [Int])")
            }
        }
        catch let error as NSError {
            print("Failed to fetch due to \(error), \(error.userInfo)")
        }
    }*/
    
    
  
    
    
    
func setupView(){
        
        let dispx = view.center.x * 2
        let dispy = view.center.y * 2
        let center = CGPoint.init(x: dispx / 2, y: dispy * 6 / 16)
        let radius1 = dispx * 3 / 16        // 3/16
        let radius2 = dispx * 33 / 192       // 11/64
        let fi = 360 * goodanswers / 2 * Double.pi / 180
        let goodx = (dispx / 2) - (radius1 * CGFloat(sin(fi)))
        let goody = (dispy * 6 / 16) + (radius1 * CGFloat(cos(fi)))
        let badx = (dispx / 2) + (radius1 * CGFloat(sin(fi)))
        let bady = (dispy * 6 / 16) - (radius1 * CGFloat(cos(fi)))
        
        smallcircle = createCircle(center: center, radius: radius2, color: gray)
        circle = createCircle(center: center, radius: radius1, color: circlecolor)
        
        
        if Int(round(goodanswers * 100)) == 0 {
            line3 = createLine(x: dispx / 2 - 1 , y: dispy * 6/16 + 1, width1: 0, width: 1, height: radius2 * 2, strokeEnd: 0, color: gray)
        }
        else {
            line3 = createLine(x: dispx / 2 - 1, y: dispy * 6/16 + 1, width1: 0, width: 1, height: radius1 * 2, strokeEnd: 0, color: circlecolor)
        }
        Animation(on: line3, from: 0, to: 1, duration: 2, beginat: 0, cp1: 0.75, cp2: 0.1, cp3: 0.28, cp4: 1, path: "strokeEnd", key: " ", mode: CAMediaTimingFillMode.removed.rawValue, revers: false, count: 1)
        
    
        if Int(round(goodanswers * 100)) != 100 && Int(round(goodanswers * 100)) != 0 {
            
            line1 = createLine(x: badx, y: bady, width1: 0, width: 1, height: dispy * 5/8 - bady, strokeEnd: 0, color: black)
            setLabel(lbl: label2, text: "Źle", aligment: .left, fontSize: 15, alpha: 0, x: badx + 3, y: dispy * 5 / 8 - 15, width: 100, height: 15)
            setLabel(lbl: label4, text: "\(100 - Int(round((goodanswers) * 100)))%", aligment: .left, fontSize: 20, alpha: 0, x: badx + 3, y: dispy * 5 / 8 - 40, width: 100, height: 20)
            Animation(on: line1, from: 0, to: 1, duration: 1, beginat: 3, cp1: 0.75, cp2: 0.1, cp3: 0.28, cp4: 1, path: "strokeEnd", key: " ", mode: CAMediaTimingFillMode.forwards.rawValue, revers: false, count: 1)
            Animation(on: circle, from: 0, to: goodanswers, duration: 2 * goodanswers, beginat: 1, cp1: 1, cp2: 0, cp3: 0.9, cp4: 1, path: "strokeEnd", key: " ", mode: CAMediaTimingFillMode.forwards.rawValue, revers: false, count: 0)
            Animation(on: smallcircle, from: goodanswers, to: 1, duration: 2*(1-goodanswers), beginat: 1 + 2*goodanswers, cp1: 0.1, cp2: 0, cp3: 0, cp4: 1, path: "strokeEnd", key: " ", mode: CAMediaTimingFillMode.forwards.rawValue, revers: false, count: 1)
            line2 = createLine(x: goodx, y: goody,width1: 0, width: 1, height: dispy * 6/8 - goody, strokeEnd: 0, color: black)
            setLabel(lbl: label1,text: "Dobrze", aligment: .left, fontSize: 15, alpha: 0, x: goodx + 3, y: dispy * 6 / 8 - 15, width: 100, height: 15)
            setLabel(lbl: label3, text: "\(Int(round(goodanswers * 100)))%", aligment: .left, fontSize: 20, alpha: 0, x: goodx + 3, y: dispy * 6 / 8 - 40, width: 100, height: 20)
            Animation(on: line2, from: 0, to: 1, duration: 1, beginat: 1 + 2*goodanswers, cp1: 0.75, cp2: 0.1, cp3: 0.28, cp4: 1, path: "strokeEnd", key: " ", mode: CAMediaTimingFillMode.forwards.rawValue, revers: false, count: 1)
            
        }
        else {
            line2 = createLine(x: goodx - 20, y: goody,width1: 0, width: 1, height: dispy * 6/8 - goody, strokeEnd: 0, color: black)
            setLabel(lbl: label1,text: "Dobrze", aligment: .left, fontSize: 15, alpha: 0, x: goodx + 3 - 20, y: dispy * 6 / 8 - 15, width: 100, height: 15)
            setLabel(lbl: label3, text: "\(Int(round(goodanswers * 100)))%", aligment: .left, fontSize: 20, alpha: 0, x: goodx + 3 - 20, y: dispy * 6 / 8 - 40, width: 100, height: 20)
            Animation(on: circle, from: 0, to: goodanswers, duration: 2 * goodanswers, beginat: 1, cp1: 5/6, cp2: 0.2, cp3: 2/6, cp4: 0.9, path: "strokeEnd", key: " ", mode: CAMediaTimingFillMode.forwards.rawValue, revers: false, count: 0)
            Animation(on: smallcircle, from: goodanswers, to: 1, duration: 2*(1-goodanswers), beginat: 1 + 2*goodanswers, cp1: 5/6, cp2: 0.2, cp3: 2/6, cp4: 0.9, path: "strokeEnd", key: " ", mode: CAMediaTimingFillMode.forwards.rawValue, revers: false, count: 1)
            Animation(on: line2, from: 0, to: 1, duration: 1, beginat: 2, cp1: 0.75, cp2: 0.1, cp3: 0.28, cp4: 1, path: "strokeEnd", key: " ", mode: CAMediaTimingFillMode.forwards.rawValue, revers: false, count: 1)
        }
        
    }
    
    
    func fetchFromCoreData(_ entityName: String , _ attributeName: String) {
        print(entityName)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.propertiesToFetch = [attributeName]
        request.resultType = .dictionaryResultType
        request.returnsObjectsAsFaults = false
        
        do {
            let context = AppDelegate.viewContext
            
            let result = try context.fetch(request)
            var x: Int = 0
            for data in result {
                print(data)
                //if let text = data.value(forKey: attributeName) {
                   // print(text)
                    //coreDataQuestionArrays.append(text as! [Int])
                    //print(coreDataQuestionArrays)
               // }
                //print(data.value(forKey: attributeName))
                //coreDataQuestionArrays.append(data.value(forKey: attributeName) as! [Int])
                //print(" dawaj info: \(data.value(forKey: attributeName) as! [Int])")
            }

            print(result)
        }
        catch let error as NSError {
            print("Failed to fetch due to \(error), \(error.userInfo)")
        }
    }

    @objc func buttonStart(sender: UIButton!) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let QuizPage: UIViewController = storyboard.instantiateViewController(withIdentifier: "QuizView")
        navigationController?.pushViewController(QuizPage, animated: true)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if Int(round(goodanswers * 100))  == 0 || Int(round(goodanswers * 100))  == 100 {
            UIView.animate(withDuration: 1, delay: 2.5, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                self.label1.alpha = 1
                self.label3.alpha = 1
            }) {(_)  in
            }
        }
        else {
        UIView.animate(withDuration: 1, delay: 1.5 + 2 * goodanswers, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.label1.alpha = 1
            self.label3.alpha = 1
        }) {(_)  in
        }
        }
        UIView.animate(withDuration: 1, delay: 3.5, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.label2.alpha = 1
            self.label4.alpha = 1
        })
    }
    
    
    func createCircle(center: CGPoint, radius: CGFloat, color: CGColor) -> CAShapeLayer{
        
        let circle = CAShapeLayer()
        let base = UIBezierPath(arcCenter: center, radius: radius, startAngle: CGFloat.pi / 2, endAngle: (CGFloat.pi / 2) + (CGFloat.pi * 2), clockwise: true)
        circle.path = base.cgPath
        circle.fillColor = UIColor(white: 1, alpha: 0).cgColor
        circle.strokeColor = color
        circle.lineWidth = radius * 2
        circle.strokeEnd = 0
        view.layer.addSublayer(circle)
        return circle
    }
    
    func createLine(x: CGFloat, y: CGFloat, width1: CGFloat ,width: CGFloat,height: CGFloat, strokeEnd: CGFloat, color: CGColor) -> CAShapeLayer{
        
        let path = UIBezierPath(roundedRect: CGRect(x: x, y: y, width: width1, height: height),cornerRadius: width / 2)
        let line  = CAShapeLayer()
        line.strokeColor = color
        line.lineWidth = width
        line.strokeEnd = strokeEnd
        line.path = path.cgPath
        view.layer.addSublayer(line)
        return line
    }
    
    func setLabel(lbl: UILabel, text: String, aligment: NSTextAlignment ,fontSize: CGFloat, alpha: CGFloat, x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat){
        
        lbl.text = text
        lbl.textAlignment = aligment
        lbl.font = UIFont.boldSystemFont(ofSize: fontSize)
        lbl.alpha = alpha
        lbl.frame = CGRect(x: x, y: y, width: width, height: height)
        view.addSubview(lbl)
    }
    
    func Animation(on: CAShapeLayer, from: Double, to: Double, duration: Double, beginat: Double, cp1: Float, cp2: Float, cp3: Float, cp4: Float, path: String, key: String, mode: String, revers: Bool, count: Float){
        
        let animation = CABasicAnimation(keyPath: path)
        animation.fromValue = from
        animation.toValue = to
        animation.duration = duration
        animation.beginTime = CACurrentMediaTime() + beginat
        animation.fillMode = CAMediaTimingFillMode(rawValue: mode)
        animation.isRemovedOnCompletion = false
        animation.timingFunction = CAMediaTimingFunction(controlPoints: cp1, cp2, cp3, cp4)
        animation.autoreverses = revers
        animation.repeatCount = count
        on.add(animation, forKey: key)
    }
    
}


extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}


extension ViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return transition
    }
    
    
}


extension UIView {
    func leftToRightAnimation(duration: TimeInterval = 0.5, completionDelegate: AnyObject? = nil) {
        // Create a CATransition object
        let leftToRightTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided
        if let delegate: AnyObject = completionDelegate {
            leftToRightTransition.delegate = delegate as? CAAnimationDelegate
        }
        
        leftToRightTransition.type = CATransitionType.push
        leftToRightTransition.subtype = CATransitionSubtype.fromRight
        leftToRightTransition.duration = duration
        leftToRightTransition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        leftToRightTransition.fillMode = CAMediaTimingFillMode.removed
        
        // Add the animation to the View's layer
        self.layer.add(leftToRightTransition, forKey: "leftToRightTransition")
    }
    
    
}








