//
//  FeedBackViewController.swift
//  MathMickV3
//
//  Created by Mati on 18.10.2018.
//  Copyright © 2018 MathMick Company. All rights reserved.
//

import UIKit
import MessageUI

class FeedBackViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }
    
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["solved@mail.com"])
            
           
            
            // Configure the fields of the interface.
            
            mail.setSubject("MathMick - FeedBack")
            
            
            
            mail.setMessageBody("<p> W MathMicku najbardziej podoba mi się... </p>", isHTML: true)
            
            present(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Theme.current.mainColor


        
        if revealViewController()  != nil {
            let button: UIButton = UIButton(type: .custom)
            button.setImage(UIImage(named: "menu-2"), for: UIControl.State.normal)
            button.widthAnchor.constraint(equalToConstant: 23).isActive = true
            button.heightAnchor.constraint(equalToConstant: 23).isActive = true
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self.revealViewController(), action: #selector(SWRevealViewController().revealToggle(_:)), for: .touchUpInside)
            
            let menuBtn = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = menuBtn
            
            //menuBtn.target = self.revealViewController()
            //menuBtn.action = #selector(SWRevealViewController().revealToggle(_:))
            
            self.view.addGestureRecognizer((revealViewController()?.panGestureRecognizer())!)
        }
        
        let alert = UIAlertController(title: "FeedBack", message: "Napisz do nas, lub oceń naszą aplikację w App Store", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Napisz do nas maila", style: .default) { (action:UIAlertAction!) in
            //pisanie maila
            self.sendEmail()
        })
        alert.addAction(UIAlertAction(title: "Oceń nas ", style: .default) { (action:UIAlertAction!) in
            //ocena w appstore
            self.rateApp(appId: "tutaj nasze appID") { success in
                print("RateApp \(success)")
            }
            
        })
        
        alert.addAction(UIAlertAction(title: "Anuluj", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
