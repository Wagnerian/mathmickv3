//
//  QuizViewController.swift
//  MathMickv2
//
//  Created by MacBook Air on 27/09/2018.
//  Copyright © 2018 MacBook Air. All rights reserved.
//

import UIKit
import CoreData
import iosMath

class QuizViewController: UIViewController {
    
    
    /////////////////////////////       VARIABLES        ///////////////////////////////////////////////////////
    
    var generator: [Int] = []
    var first: Int? = nil
    var randCategory = Int()
    var result1: [Array<String>] = []
    var result2: [Array<String>] = []
    var result3: [Array<String>] = []
    var result4: [Array<String>] = []
    var result5: [Array<String>] = []
    var results: [Array<Array<String>>] = [[[]]]
    var goodAnswerPlacement: Int!
    var badAnswerPlacement: Bool!
    var label:[UILabel] = [UILabel(),UILabel(),UILabel(),UILabel()]
    var answearLabel:[MTMathUILabel] = [MTMathUILabel(),MTMathUILabel(),MTMathUILabel(),MTMathUILabel()]
    var button:[UIButton] = [UIButton(),UIButton(),UIButton(),UIButton()]
    var randCat: [Int] = []
    var coreDataQuestionArrays: [Array<Int>] = []
    var timer = Timer()
    var counts: Int16 = Int16(UserDefaults.standard.integer(forKey: "Stoper"))

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    ///////////////////////////////     CONSTANTS     ///////////////////////////////////////////////////
    
    let CoreDataContainer = AppDelegate.persistentContainer
    let context = AppDelegate.viewContext
    let delegate = UIApplication.shared.delegate as? AppDelegate
    let defaults = UserDefaults.standard
    let categories = ["Algebra" , "Funkcje" , "Trygonometria" , "Geometria" , "Prawdopodobienstwo"]
    let goodCounterStrings = ["AgoodAnswerCounter" , "FgoodAnswerCounter" , "TgoodAnswerCounter" , "GgoodAnswerCounter" , "PgoodAnswerCounter"]
    let badCounterStrings = ["AbadAnswerCounter" , "FbadAnswerCounter" , "TbadAnswerCounter" , "GbadAnswerCounter" , "PbadAnswerCounter"]

    let Question: MTMathUILabel = {
        let lbl = MTMathUILabel()
        lbl.backgroundColor = Theme.current.mainColor
        lbl.textColor = UIColor.black
        lbl.textAlignment = .center
        lbl.fontSize = 17.0
        //let fontManager = MTFontManager()
        //lbl.font = fontManager.xitsFont(withSize: 25)
        return lbl
    }()
    
    let nextButton: UIButton = {
        let button = UIButton()
        button.isEnabled = false
        button.backgroundColor = UIColor(red: 193, green: 202, blue: 192, alpha: 0)
        button.addTarget(self, action: #selector(RandomCategoryGenerator), for: .touchUpInside)
        return button
    }()
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    
  
    
    ///////////////////////////////     VIEW DID LOAD     ///////////////////////////////////////////////////
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Theme.current.mainColor

        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        if screenWidth == 320.0 { Question.fontSize = 16.0}
        else if screenWidth == 375.0 { Question.fontSize = 20.0}
        else if screenWidth == 414.0 { Question.fontSize = 22.0}

        print(screenWidth)
        
        odczyt()
        RandomCategoryGenerator()
        

       MTMathAtomFactory.addLatexSymbol("ee", value: MTMathAtomFactory.operator(withName: "ę", limits: false))
        
        MTMathAtomFactory.addLatexSymbol("aa", value: MTMathAtomFactory.operator(withName: "ą", limits: false))
        MTMathAtomFactory.addLatexSymbol("nn", value: MTMathAtomFactory.operator(withName: "ń", limits: false))
        
        NotificationCenter.default.addObserver(self, selector: #selector(stoppingTheTimer), name:UIApplication.didEnterBackgroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(startingTheTimer), name:UIApplication.didBecomeActiveNotification, object: nil)
        
        let boardButton: UIButton = UIButton(type: .custom)
        boardButton.setImage(UIImage(named: "board"), for: UIControl.State.normal)
        boardButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        boardButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        boardButton.translatesAutoresizingMaskIntoConstraints = false
        boardButton.addTarget(self, action: #selector(clicked), for: .touchUpInside)
        
        let boardBtn = UIBarButtonItem(customView: boardButton)
        self.navigationItem.rightBarButtonItem = boardBtn
        
        
    }

    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /////////////////////////      GENERATING NEW QUESTIONS FUNCTION      ///////////////////////////////////

    
    @objc func stoppingTheTimer(notification : NSNotification) {
        print("stop method called")
        timer.invalidate()
        UserDefaults.standard.set(counts, forKey: "Stoper")

    }
    @objc func startingTheTimer(notification : NSNotification) {
        print("start method called")
        
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            
            self.counts += 1
            
            print(self.counts)
            
            //zapisać największy runCount i dodać do danych statystyk przy konkretnej kategorii z pytania
 
        }

    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    
    /////////////////////////      GENERATING NEW QUESTIONS FUNCTION      ///////////////////////////////////
    
    
    @objc func newQuestion() {
        
        Question.frame = CGRect(x: 0, y: view.center.y * 0.75 / 10, width: view.center.x * 2, height: view.center.y * 9 / 10)
        Question.layer.cornerRadius = view.center.x/6
        
        view.addSubview(Question)
        
        let dispbtnx = view.center.x
        let dispbtny = view.center.y
        let placebtn: [[CGFloat]] = [[ dispbtnx * 2 / 20 , dispbtny , dispbtnx*45/20 , dispbtny/5 ] , [ dispbtnx * 2 / 20 , dispbtny + dispbtny / 4 , dispbtnx*45/20 , dispbtny/5 ] , [ dispbtnx * 2 / 20 , dispbtny + dispbtny / 2 , dispbtnx*45/20 , dispbtny/5 ] , [ dispbtnx * 2 / 20 , dispbtny + dispbtny * 3 / 4 , dispbtnx*45/20 , dispbtny/5 ]]
        
        for index in 0...3 {
            createButton(xPosition: placebtn[index][0], yPosition: placebtn[index][1], width1: placebtn[index][2], height1: placebtn[index][3], i: index)
        }
        
        
        view.sendSubviewToBack(nextButton)
        nextButton.isEnabled = false

        for k in 1...4 {
            button[k - 1].isEnabled = false
        }
        
        if coreDataQuestionArrays[randCategory].count > 0 {
            first = coreDataQuestionArrays[randCategory].first
            print("First: \(first!)")
            print(categories[randCategory])
            print(coreDataQuestionArrays[randCategory])
            print(coreDataQuestionArrays[randCategory].count)
            print("rezultaty: \(results[randCategory][first!])")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                
                self.Question.latex = self.results[self.randCategory][self.first! - 1][0]
                
                self.generator = self.GenerateRandomNumbers(1, 5, 4)
                
                for x in 0...3 {
                    if self.generator[x] == 4 {
                        self.goodAnswerPlacement = x
                    }
                }
                for i in 1...4 {
                    self.answearLabel[i - 1].latex = self.results[self.randCategory][self.first! - 1][self.generator[i - 1]]
                    self.button[i-1].backgroundColor = Theme.current.ButtonsBackColor
                    //self.button[i-1].backgroundColor = Theme.current.categoryBackColor

                }
            }
            
        }
        else {
            var x: [Int] = []
            if randCategory == 0 {  x = [1 , 2 , 3 , 4] }
            else if randCategory == 1 {  x = [0 , 2 , 3 , 4] }
            else if randCategory == 2 {  x = [0 , 1 , 3 , 4] }
            else if randCategory == 3 {  x = [0 , 1 , 2 , 4] }
            else if randCategory == 4 {  x = [0 , 1 , 2 , 3] }
            randCategory = x.randomElement()!
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            for k in 1...4{
                self.button[k - 1].isEnabled = true
            }
        }
        
        
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    ////////////////     CHECKING AND COUNTING GOOD AND BAD ANSWEARS FUNCTION     ///////////////////////////
    
    
    @objc func odp(_ sender: UIButton) {
        
        timer.invalidate()
        
        let algebra = Algebra(context: self.context)
        let funkcje = Funkcje(context: self.context)
        let trygonometria = Trygonometria(context: self.context)
        let geometria = Geometria(context: self.context)
        let prawdopodobienstwo = Prawdopodobienstwo(context: self.context)
        
        
        
        for x in 0...3 {
            button[x].isEnabled = false
        }
        
        if sender.tag == (goodAnswerPlacement + 1) {
            //////////////  GOOD ANSWEAR   /////////////
            sender.backgroundColor = UIColor(red: 179/255, green: 222/255, blue: 129/255, alpha: 1)
            
            
            if coreDataQuestionArrays[randCategory].count >= 1 {
                let goodAnswearCounter = UserDefaults.standard.integer(forKey: goodCounterStrings[randCategory])
                let counter: Int = goodAnswearCounter + 1
                self.defaults.set(counter, forKey: goodCounterStrings[randCategory])
                self.defaults.synchronize()
            }
            
        }
        else {
            //////////////  WRONG ANSWEAR   /////////////
            sender.backgroundColor = UIColor(red: 235/255, green: 94/255, blue: 48/255, alpha: 1)
            self.badAnswerPlacement = false
            
            if coreDataQuestionArrays[randCategory].count >= 1 {
                let badAnswearCounter = UserDefaults.standard.integer(forKey: badCounterStrings[randCategory])
                let counter: Int = badAnswearCounter + 1
                self.defaults.set(counter, forKey: badCounterStrings[randCategory])
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                
                if self.badAnswerPlacement == true {
                    if self.button[0].tag == (self.goodAnswerPlacement + 1) {
                        self.button[0].backgroundColor = UIColor(red: 179/255, green: 222/255, blue: 129/255, alpha: 1)
                    }
                    else if self.button[1].tag == (self.goodAnswerPlacement + 1) {
                        self.button[1].backgroundColor = UIColor(red: 179/255, green: 222/255, blue: 129/255, alpha: 1)
                    }
                    else if self.button[2].tag == (self.goodAnswerPlacement + 1) {
                        self.button[2].backgroundColor = UIColor(red: 179/255, green: 222/255, blue: 129/255, alpha: 1)
                    }
                    else if self.button[3].tag == (self.goodAnswerPlacement + 1) {
                        self.button[3].backgroundColor = UIColor(red: 179/255, green: 222/255, blue: 129/255, alpha: 1)
                    }
                }
            }
            
            badAnswerPlacement = true
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.1) {
            
            self.nextButton.frame = CGRect(x: 0 , y: 0 , width: self.view.center.x * 2, height: self.view.center.y * 2)
            self.view.addSubview(self.nextButton)
            self.view.sendSubviewToBack(self.nextButton)
            
            if self.coreDataQuestionArrays[self.randCategory].count >= 1 {
                self.coreDataQuestionArrays[self.randCategory].remove(at:0)
                //self.ClearCoreData(self.categories[self.randCategory])

                if self.randCategory == 0 {
                    guard let task = Algebra.instance(with: "questions")
                        else { return }
                    task.updatequestions(with: self.coreDataQuestionArrays[self.randCategory]) }
                else if self.randCategory == 1 {
                    guard let task = Funkcje.instance(with: "questions")
                        else { return }
                    task.updatequestions(with: self.coreDataQuestionArrays[self.randCategory])
                }
                else if self.randCategory == 2 {
                    guard let task = Trygonometria.instance(with: "questions")
                        else { return }
                    task.updatequestions(with: self.coreDataQuestionArrays[self.randCategory])
                }
                else if self.randCategory == 3 {
                    guard let task = Geometria.instance(with: "questions")
                        else { return }
                    task.updatequestions(with: self.coreDataQuestionArrays[self.randCategory])
                }
                else if self.randCategory == 4 {
                    guard let task = Prawdopodobienstwo.instance(with: "questions")
                        else { return }
                    task.updatequestions(with: self.coreDataQuestionArrays[self.randCategory])
                }
                
                self.saveInCoreData(self.categories[self.randCategory], self.counts , "czasOdp")
                //self.saveInCoreData(self.categories[self.randCategory], self.coreDataQuestionArrays[self.randCategory] , "questions")
                self.view.bringSubviewToFront(self.nextButton)
                self.nextButton.isEnabled = true
            }
           /* else if self.coreDataQuestionArrays[self.randCategory].count == 1 {
                self.coreDataQuestionArrays[self.randCategory].remove(at:0)
                self.ClearCoreData(self.categories[self.randCategory])

                self.saveInCoreData(self.categories[self.randCategory], self.counts , "czasOdp")
                self.saveInCoreData(self.categories[self.randCategory], self.coreDataQuestionArrays[self.randCategory] , "questions")
                print("\(self.categories[self.randCategory]): Ostatnie pytanie rozwiązane")
                self.view.bringSubviewToFront(self.nextButton)
                self.nextButton.isEnabled = true
                
            }*/
            
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    
     
    ///////////////////////////       GOING BACK TO THE MAIN SCREEN FUNCTION      //////////////////////////

    @IBAction func BackButton(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let MenuView = storyboard.instantiateViewController(withIdentifier: "menuNavigation")
        self.navigationController?.revealViewController()?.pushFrontViewController(MenuView, animated: true)
        timer.invalidate()
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    
    ///////////////////////////       GOING TO THE FORMULAS PAGE VIEW      //////////////////////////
    
    @objc func clicked(sender: UIButton!) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let FormulasPage = storyboard.instantiateViewController(withIdentifier: "WzoryQuizPage")
        self.navigationController?.pushViewController(FormulasPage, animated: true)
        
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    ///////////////////////////  CREATOR OF  A, B, C, D, BUTTONS AND THEY LABELS //////////////////////////
    
    
    func createButton(xPosition: CGFloat, yPosition: CGFloat, width1 : CGFloat, height1 : CGFloat, i: Int) {
        
        let text = ["A" , "B" , "C" , "D"]
        
        button[i] = {
            let button = UIButton()
            button.backgroundColor = Theme.current.ButtonsBackColor
            button.tag = i + 1
            button.addTarget(self.button[i], action: #selector(odp), for: .touchUpInside)
            return button
        }()
        
        label[i] = {
            let lbl = UILabel()
            lbl.textAlignment = .left
            lbl.text = text[i]
            lbl.font = UIFont.boldSystemFont(ofSize: 24)
            lbl.textColor = Theme.current.mainColor
            return lbl
        }()
        
        //button[i].layer.borderColor = Theme.current.ButtonsBackColor.cgColor
        //button[i].layer.borderWidth = 3.0
        button[i].frame = CGRect(x: xPosition, y: yPosition , width: width1, height: height1)
        button[i].layer.cornerRadius = view.center.x/6
        view.addSubview(button[i])
        
        label[i].frame = CGRect(x: xPosition, y: 0, width: width1 - xPosition, height: height1)
        button[i].addSubview(label[i])
        
        answearLabel[i].frame = CGRect(x: xPosition * 3 / 2, y: 0, width: width1 - xPosition, height: height1)
        answearLabel[i].fontSize = 15.0
        answearLabel[i].textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        label[i].addSubview(answearLabel[i])
        
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /////////////////      Czytanie z pliku pytań i odpowiedzi  tablica[][]   ////////////////////////////////////
    
    func odczyt() {
        
        results = [result1 , result2 , result3 , result4 , result5]
        for kategoria in categories {
            
            let fileURL = Bundle.main.path(forResource: kategoria, ofType: "txt")
            var readString = ""
            do {
                readString = try String(contentsOfFile: fileURL! , encoding: String.Encoding.utf8)
                
                let myStrings = readString.components(separatedBy: "\n")
                for row in myStrings {
                    let columns = row.components(separatedBy: "#")
                    
                    if kategoria == categories[0] {
                        results[0].append(columns)
                    }
                    else if kategoria == categories[1] {
                        results[1].append(columns)
                    }
                    else if kategoria == categories[2] {
                        results[2].append(columns)
                    }
                    else if kategoria == categories[3] {
                        results[3].append(columns)
                    }
                    else if kategoria == categories[4] {
                        results[4].append(columns)
                    }
                }
            }
            catch let error as NSError {
                print("failed to read from file bazapytan")
                print(error)
            }
        }
        
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    //////////////////////////   Randomizators      ////////////////////////////////
    
    // 1 //
    
    func randomNumber(range: ClosedRange<Int> = 0...4) -> Int {
        let min = range.lowerBound
        let max = range.upperBound
        return Int(arc4random_uniform(UInt32(1 + max - min))) + min
    }
    
    // 2 //
    
    func GenerateRandomNumbers(_ from: Int, _ to: Int, _ howmany: Int?) -> [Int] {
        var myRandomNumbers: [Int] = []
        var numberOfNumbers = howmany
        
        let lower = UInt32(from)
        let higher = UInt32(to)
        
        if numberOfNumbers == nil || numberOfNumbers! > (to - from) + 1 {
            numberOfNumbers = (to - from) + 1
        }
        
        while myRandomNumbers.count != numberOfNumbers {
            let myNumber = arc4random_uniform(higher - lower) + lower
            
            if !myRandomNumbers.contains(Int(myNumber)) {
                myRandomNumbers.append(Int(myNumber))
            }
        }
        
        return myRandomNumbers
        
    }
    
    // 3 //
    
    @objc func RandomCategoryGenerator() {
        coreDataQuestionArrays.removeAll()
        for category in categories {
            fetchFromCoreData(category, "questions")
        }
        
        randCat = []
        let categoryNames = ["AlgebraCategory" , "FunkcjeCategory" , "TrygonometriaCategory" , "GeometriaCategory" , "PrawdopodobienstwoCategory"]
        for index in 0...4 {
            print("prosze: \(coreDataQuestionArrays[index].count)")
            if coreDataQuestionArrays[index].count > 0 {
                let categoryChecker = defaults.bool(forKey: categoryNames[index])
                if categoryChecker == false {
                    randCat.append(index)
                }
            }
            
        }
        
        Question.removeFromSuperview()
        for index in 0...3 {
            button[index].removeFromSuperview()
        }
        
        if randCat.count != 0 {
            counts = 0
            randCategory = randCat.randomElement()!
            print("kategoria nr: \(randCategory)")
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
                self.counts += 1
                print(self.counts)
                //zapisać największy runCount i dodać do danych statystyk przy konkretnej kategorii z pytania
            }

            newQuestion()
        }
        else if randCat.count == 0 && (coreDataQuestionArrays[0].count > 0 || coreDataQuestionArrays[1].count > 0 || coreDataQuestionArrays[2].count > 0 || coreDataQuestionArrays[3].count > 0 || coreDataQuestionArrays[4].count > 0){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                print("Brak wiecej pytań")
                
                let dispbtnx = self.view.center.x
                let dispbtny = self.view.center.y
                self.Question.layer.borderColor = Theme.current.mainColor.cgColor
                self.Question.layer.borderWidth = 2.0
                //self.Question.numberOfLines = 5
                self.Question.frame = CGRect(x: 0, y: dispbtny, width: dispbtnx * 2, height: dispbtny/5)
                self.Question.textAlignment = .center
                self.Question.latex = " Brak \\ wi \\ee cej \\ pyta \\acute n  \\ z \\\\ wybranych \\ kategorii "
                self.Question.fontSize = 20.0
                print(MTMathAtomFactory.supportedLatexSymbolNames()[12])

                self.view.addSubview(self.Question)

/*
                MTMathAtomFactory.fontStyle(withName: "cal")
                self.Question.latex = "Brak \\ wi \\le  cej \\ pyta \\nn \\ z \\ wybranych \\ kategorii"

                for k in 1...4{
                    self.button[k-1].isEnabled = false
                    self.button[k-1].backgroundColor = UIColor(red: 91/255, green: 112/255, blue: 101/255, alpha: 1)
                    self.answearLabel[k-1].latex = ""
                }
                
                */
            }
        }
        else if randCat.count == 0 && coreDataQuestionArrays[0].count == 0 && coreDataQuestionArrays[1].count == 0 && coreDataQuestionArrays[2].count == 0 && coreDataQuestionArrays[3].count == 0 && coreDataQuestionArrays[4].count == 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                
                let dispbtnx = self.view.center.x
                let dispbtny = self.view.center.y
                self.Question.frame = CGRect(x: 0, y: dispbtny, width: dispbtnx * 2, height: dispbtny/5)
                self.Question.layer.borderColor = Theme.current.mainColor.cgColor
                self.Question.layer.borderWidth = 2.0
                self.Question.latex = "Koniec \\ Pytan"
                self.Question.fontSize = 22.0
                self.view.addSubview(self.Question)
                //for k in 1...4{
                    //self.button[k-1].removeFromSuperview()
                    //self.button[k-1].isEnabled = false
                    //self.button[k-1].backgroundColor = UIColor(red: 91/255, green: 112/255, blue: 101/255, alpha: 1)
                    //self.answearLabel[k-1].latex = ""
                //}
                
            }
            
            
        }
        print(randCat)
        
        
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////      CORE DATA     ///////////////////////////////////////////////
    
    func saveInCoreData(_ entityName: String , _ value: Any , _ attributeName: String) {
        
        let context = AppDelegate.viewContext
        
        let entity = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context)
        entity.setValue(value, forKey: attributeName)
        
        let delegate = UIApplication.shared.delegate as? AppDelegate
        
        do {
            try delegate?.saveContext()
        }
        catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
    }
    
    func fetchFromCoreData(_ entityName: String , _ attributeName: String) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                if let text = data.value(forKey: attributeName) {
                    print(text)
                    coreDataQuestionArrays.append(text as! [Int])
                    print(coreDataQuestionArrays)
                }
                //print(data.value(forKey: attributeName))
                //coreDataQuestionArrays.append(data.value(forKey: attributeName) as! [Int])
                //print(" dawaj info: \(data.value(forKey: attributeName) as! [Int])")
            }
        }
        catch let error as NSError {
            print("Failed to fetch due to \(error), \(error.userInfo)")
        }
    }
    
    func ClearCoreData(_ entity: String) {
        
        let moc = delegate?.persistentContainer.viewContext
        
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entity)
        do {
            
            if let allObjects = try moc?.fetch(request) as? [NSManagedObject] {
                
                for object in allObjects {
                    moc?.delete(object)
                }
            }
        }
        catch let error as NSError {
            print(error)
        }
        
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
}


extension Algebra {

    // to get an instance with specific name
    class func instance(with name: String) -> Algebra? {
        let request: NSFetchRequest<Algebra> = Algebra.fetchRequest()

        // create an NSPredicate to get the instance you want to make change
        let predicate = NSPredicate(format: "czasOdp = %@", name)
        request.predicate = predicate
        
        do {
            let tasks = try AppDelegate.viewContext.fetch(request)
            return tasks.first
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    // to update an instance with specific name
    func updatequestions(with questions: [Int]) {
        self.questions = questions
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    // to delete an instance
    func delete() {
        AppDelegate.viewContext.delete(self)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
}


extension Funkcje {
    
    // to get an instance with specific name
    class func instance(with name: String) -> Funkcje? {
        let request: NSFetchRequest<Funkcje> = Funkcje.fetchRequest()
        
        // create an NSPredicate to get the instance you want to make change
        let predicate = NSPredicate(format: "czasOdp = %@", name)
        request.predicate = predicate
        
        do {
            let tasks = try AppDelegate.viewContext.fetch(request)
            return tasks.first
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    // to update an instance with specific name
    func updatequestions(with questions: [Int]) {
        self.questions = questions
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    // to delete an instance
    func delete() {
        AppDelegate.viewContext.delete(self)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
}


extension Trygonometria {
    
    // to get an instance with specific name
    class func instance(with name: String) -> Trygonometria? {
        let request: NSFetchRequest<Trygonometria> = Trygonometria.fetchRequest()
        
        // create an NSPredicate to get the instance you want to make change
        let predicate = NSPredicate(format: "czasOdp = %@", name)
        request.predicate = predicate
        
        do {
            let tasks = try AppDelegate.viewContext.fetch(request)
            return tasks.first
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    // to update an instance with specific name
    func updatequestions(with questions: [Int]) {
        self.questions = questions
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    // to delete an instance
    func delete() {
        AppDelegate.viewContext.delete(self)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
}


extension Geometria {
    
    // to get an instance with specific name
    class func instance(with name: String) -> Geometria? {
        let request: NSFetchRequest<Geometria> = Geometria.fetchRequest()
        
        // create an NSPredicate to get the instance you want to make change
        let predicate = NSPredicate(format: "czasOdp = %@", name)
        request.predicate = predicate
        
        do {
            let tasks = try AppDelegate.viewContext.fetch(request)
            return tasks.first
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    // to update an instance with specific name
    func updatequestions(with questions: [Int]) {
        self.questions = questions
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    // to delete an instance
    func delete() {
        AppDelegate.viewContext.delete(self)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
}


extension Prawdopodobienstwo {
    
    // to get an instance with specific name
    class func instance(with name: String) -> Prawdopodobienstwo? {
        let request: NSFetchRequest<Prawdopodobienstwo> = Prawdopodobienstwo.fetchRequest()
        
        // create an NSPredicate to get the instance you want to make change
        let predicate = NSPredicate(format: "czasOdp = %@", name)
        request.predicate = predicate
        
        do {
            let tasks = try AppDelegate.viewContext.fetch(request)
            return tasks.first
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    // to update an instance with specific name
    func updatequestions(with questions: [Int]) {
        self.questions = questions
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    // to delete an instance
    func delete() {
        AppDelegate.viewContext.delete(self)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
}
